from setuptools import setup

setup(
    name = 'doit',
    version = '0.1.0',
    packages = ['doit'],
    entry_points = {
        'console_scripts': [
            'doit = doit.__main__:main'
        ]
})
